/*
 * Author: 
 * Date: 2021
 * Description: Template file for CS475
 * FileName:
 */

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
    //start the time tracking.
    double time0 = omp_get_wtime();

    //user code starts here.
    

    //user code ends here.


    //end the time tracking
    double time1 = omp_get_wtime();

    //print out the time calulated, the formatting option helps print 
    //the long float, or double.
    fprintf(stderr, "Elapsed time = %10.2lf microseconds\n", 1000000. * (time1 - time0));


    return 0;
}
