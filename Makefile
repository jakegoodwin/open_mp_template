CC = gcc
CFLAGS = -std=c99 -g -fopenmp -lm -O3

all:
	$(CC) $(CFLAGS) -o run_me main.c

clean:
	rm ./run_me
